import Foundation
import Publish
import Plot

// This type acts as the configuration for your website.
struct DianaWebsite: Website {
    enum SectionID: String, WebsiteSectionID {
        case verein
        case news
        case sport
        case disziplinen
        case galerie
        case chronik
//        case schuetzenjugend
        case kontakt
    }

    struct ItemMetadata: WebsiteItemMetadata {
        // Add any site-specific metadata that you want to use here.
        var thumbnail: String?
        var backgroundImage: String?

        var subTitle: String?
        var bannerImage: String?
        var htmlTag: String?

        var showFullAsDetail: Bool?

        var featureOnHome: Bool?

        var albumPath: String?
        var hideContent: Bool?

        var hideTitle: Bool?

        var hideBorder: Bool?

        var subMenuTitle: String?
        var subMenuOrder: Int?
        var subMenuDefault: Bool?
    }

    // Update these properties to configure your website:
    var url = URL(string: "https://your-website-url.com")!
    var name = "SV Diana Dettingen"
    var description = ""
    var language: Language { .german }
    var imagePath: Path? { nil }
}

// This will generate your website using the built-in Foundation theme:
try DianaWebsite().publish(withTheme: .diana)
