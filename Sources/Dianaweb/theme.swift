/**
*  theme.swift
*/

import Plot
import Files
import Publish

extension Content.Body {
    func splitHTML() -> [Self] {
        return html.components(separatedBy: "<hr>").map{Content.Body(stringLiteral: $0)}
    }
}


extension Theme where Site == DianaWebsite {
    /// The default "Foundation" theme that Publish ships with, a very
    /// basic theme mostly implemented for demonstration purposes.
    static var diana: Self {
        Theme(
            htmlFactory: DianaHTMLFactory(),
            resourcePaths: ["Resources/DianaTheme/styles.css"]
        )
    }





    private struct DianaHTMLFactory: HTMLFactory {
        func makeIndexHTML(for index: Index,
                           context: PublishingContext<Site>) throws -> HTML {
            HTML(
                .lang(context.site.language),
                .head(for: index, on: context.site),
                .body {
                    SiteHeader(context: context, selectedSelectionID: nil)
                    Wrapper {
                        H1(index.title)
                        Div {
                            Image("/backgrounds/9.jpg")
                        }.class("banner")
                    }

                    let items = context.allItems(
                        sortedBy: \.date,
                        order: .descending
                    ).filter{$0.metadata.featureOnHome == true}

//                    Wrapper {
//                        H2("Termine")
//                    }
                    for item in items.filter{$0.metadata.showFullAsDetail == true}.sorted{$0.metadata.subMenuOrder ?? 0 < $1.metadata.subMenuOrder ?? 0} {
                        makeItem(for: item, context: context)
                    }

                    Wrapper {
                        H2("News")
                        ItemList(
                            items: items.filter { $0.metadata.showFullAsDetail ?? false == false},
//                            .filter{$0.metadata.subMenuTitle == nil && $0.metadata.showFullAsDetail ?? false == false},
                            site: context.site
                        )
                    }
                    SiteFooter()
                }
            )
        }

        // Folder
        func makeSectionHTML(for section: Section<Site>,
                             context: PublishingContext<Site>) throws -> HTML {
            HTML(
                .lang(context.site.language),
                .head(for: section, on: context.site),
                .body {
                    SiteHeader(context: context, selectedSelectionID: section.id, selectedPath: section.path)
                    for item in section.items.filter{$0.metadata.showFullAsDetail == true}.sorted{$0.metadata.subMenuOrder ?? 0 < $1.metadata.subMenuOrder ?? 0} {
                        makeItem(for: item, context: context)
                    }
                    let subItems = section.items.filter{$0.metadata.showFullAsDetail ?? false == false}
                    if subItems.count > 0 {
                        Wrapper {
                            H1(section.title)
                            ItemList(items: subItems, site: context.site)
                        }
                    }
                    SiteFooter()
                }
            )
        }

        private func makeItem(for item: Item<Site>, context: PublishingContext<Site>) -> Component {
            Wrapper {
                if let htmlTag = item.metadata.htmlTag {
                    Link("", url: "").attribute(named: "name", value: htmlTag)
                }
                if item.metadata.hideTitle != true {
                    H1(item.title)
                }
                if let subTitle = item.metadata.subTitle {
                    Paragraph(subTitle).class("subtitle")
                }
                if let bannerImage = item.metadata.bannerImage {
                    Div {
                        Image(bannerImage)
                    }.class("banner")
                }
                else if let bannerImage = item.metadata.thumbnail {
                    Div {
                        Image(bannerImage)
                    }.class("banner")
                }
                if item.metadata.hideContent ?? false == false {
                    for body in item.content.body.splitHTML() {
                        Wrapper {
                            Article {
                                Div(body).class("content")
                                if item.tags.count > 0 {
                                    Span("Tags: ")
                                    ItemTagList(item: item, site: context.site)
                                }
                            }
                        }.class("body-wrapper").class(item.metadata.hideBorder == true ? "noBorderWrapper": "")
                    }
                }
                Div {
                    if let albumPath = item.metadata.albumPath {
                        let resourcesFolder = try! Folder(path: "./Resources/")
                        let folder = try! resourcesFolder.subfolder(at: albumPath)
                        for file in folder.files {
                            Div {
                                Image("/"+file.path(relativeTo: resourcesFolder))
                            }.class("item")
                        }
                    }
                }.class("album")
            }
        }

        // Single Content Page
        func makeItemHTML(for item: Item<Site>,
                          context: PublishingContext<Site>) throws -> HTML {
            HTML(
                .lang(context.site.language),
                .head(for: item, on: context.site),
                .body(
                    .class("item-page"),
                    .components {
                        SiteHeader(context: context, selectedSelectionID: item.sectionID, selectedPath: item.path)
                        makeItem(for: item, context: context)
                        SiteFooter()
                    }
                )
            )
        }

        func makePageHTML(for page: Page,
                          context: PublishingContext<Site>) throws -> HTML {
            HTML(
                .lang(context.site.language),
                .head(for: page, on: context.site),
                .body {
                    SiteHeader(context: context, selectedSelectionID: nil, selectedPath: page.path)
                    Wrapper {
                        for (i, body) in page.body.splitHTML().enumerated() {
                            if i == 0 {
                                    H1(page.title)
                                }
                                Wrapper(body).class("body-wrapper")
                        }
                    }
                    SiteFooter()

                }
            )
        }

        func makeTagListHTML(for page: TagListPage,
                             context: PublishingContext<Site>) throws -> HTML? {
            HTML(
                .lang(context.site.language),
                .head(for: page, on: context.site),
                .body {
                    SiteHeader(context: context, selectedSelectionID: nil)
                    Wrapper {
                        H1("Tags")
                        List(page.tags.sorted()) { tag in
                            ListItem {
                                Link(tag.string,
                                     url: context.site.path(for: tag).absoluteString
                                )
                            }
                            .class("tag")
                        }
                        .class("all-tags")
                    }
                    SiteFooter()
                }
            )
        }

        func makeTagDetailsHTML(for page: TagDetailsPage,
                                context: PublishingContext<Site>) throws -> HTML? {
            HTML(
                .lang(context.site.language),
                .head(for: page, on: context.site),
                .body {
                    SiteHeader(context: context, selectedSelectionID: nil)
                    Wrapper {
                        H1 {
                            Text("Tag ")
                            Span(page.tag.string).class("tag")
                        }

                        Link("Alle Tags",
                            url: context.site.tagListPath.absoluteString
                        )
                        .class("browse-all")

                        ItemList(
                            items: context.items(
                                taggedWith: page.tag,
                                sortedBy: \.date,
                                order: .descending
                            ),
                            site: context.site
                        )
                    }
                    SiteFooter()
                }
            )
        }
    }

    private struct Wrapper: ComponentContainer {
        @ComponentBuilder var content: ContentProvider

        var body: Component {
            Div(content: content).class("wrapper")
        }
    }

    private struct SiteHeader: Component {
        var context: PublishingContext<Site>
        var selectedSelectionID: Site.SectionID?
        var selectedPath: Path?

        var body: Component {
            Header {
                Wrapper {
                    Navigation {
                        Link("", url: "/").class("diana-logo")
                        List {
                            for sectionID in Site.SectionID.allCases {
                                let section = context.sections[sectionID]
                                let isActive = selectedPath?.absoluteString.contains(section.path.absoluteString) ?? false

                                if let defaultSubItem = section.items.first(where: {$0.metadata.subMenuDefault == true}) {
                                    Link(section.title, url: defaultSubItem.path.absoluteString)
                                        .class(isActive ? "selected" : "")
                                }
                                else {
                                    Link(section.title, url: section.path.absoluteString)
                                        .class(isActive ? "selected" : "")
                                }
                            }
                            Link("Live", url: "https://live.diana-dettingen.de")
                        }
                    }

                    if let selectedSelectionID = selectedSelectionID {
                        let section = context.sections[selectedSelectionID]
                        if section.items.filter{$0.metadata.subMenuTitle != nil}.count > 0 {
                            ItemNavList(items: section.items, selectedPath: selectedPath, site: context.site)
                        }
                    }
                }
            }
        }
    }

    private struct ItemList: Component {
        var items: [Item<Site>]
        var site: Site

        var body: Component {
            List(items) { item in
                Article {
                    if let thumbnail = item.metadata.thumbnail {
                        Div {
                            Image(thumbnail)
                        }.class("thumbnail")
                    }
                    Div {
                        H1(Link(item.title, url: item.path.absoluteString))
                        ItemTagList(item: item, site: site)
                        Paragraph(item.description)
                    }.class("item-preview")
                }
            }
            .class("item-list")
        }
    }

    private struct ItemNavList: Component {
        var items: [Item<Site>]
        var selectedPath: Path?
        var site: Site


        var body: Component {
            Navigation {
                List {
                    for item in items.sorted{$0.metadata.subMenuOrder ?? 0 < $1.metadata.subMenuOrder ?? 0} {
                        if let title = item.metadata.subMenuTitle {
                            let isActive = selectedPath?.absoluteString.contains(item.path.absoluteString) ?? false
                            Link(title, url: item.path.absoluteString)
                                .class(isActive ? "selected" : "")
                        }
                    }
                }
            }
        }
    }

    private struct ItemTagList: Component {
        var item: Item<Site>
        var site: Site

        var body: Component {
            List(item.tags) { tag in
                Link(tag.string, url: site.path(for: tag).absoluteString)
            }
            .class("tag-list")
        }
    }

    private struct SiteFooter: Component {
        var body: Component {
            Footer {
                Paragraph {
                    Text("©2021 Schützenverein Diana Dettingen")
                    Text(" | ")
                    Link("Impressum", url: "/impressum/")
                    Text(" | ")
                    Link("Datenschutz", url: "/datenschutz/")
                }
            }
        }
    }

}
