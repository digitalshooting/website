---
title: Kleinkaliber
subTitle: präzise auf 50 Meter
bannerImage: /images/kk_2.png
showFullAsDetail: true
subMenuOrder: 3
---
## Fakten
- 8 konventionellen Schießstände - 50m
- 3 ausgebildete Jugendtrainer
- 3 Fachtrainer 

Das vor 5 Jahren als Versuch mit 3 Schülern gestartete Projekt Luftgewehr 3 Stellung hat sich so erfolgreich entwickelt, das wir uns dazu entschlossen haben, die Kleinkaliber Tradition bei Diana Dettingen wieder aufleben zu lassen.

Das 3 Stellungsschießen mit dem Luftgewehr ist der Grundbaustein für das Kleinkaliber schießen. Da unsere Jungschützen in den vergangenen Jahren reichlich Erfahrungen sammeln konnten, war der Gedanke naheliegend mit dem Kleinkaliberschießen weiter zu machen. Schnell war eine Trainingsgruppe von 6 Jungschützen gefunden.

Um das Ganze langsam an zu gehen, starteten wir im Jahr 2013 mit 6 Jungschützen nur mit der Disziplin 60 Schuss liegend. Gleich im ersten Jahr wurden mehrere Gaumeistertitel und die Qualifikation für die Bayerischen Meisterschaften geschafft.

Seit dieser erfolgreichen Wiederbelebung des KK Schießens im Jahr 2013 konnten die Starterzahlen für die diversen Meisterschaften kontinuierlich gesteigert werden. Im vergangen Jahr gingen 13 Jungschützen von Diana Dettingen in München in den Disziplinen 60 Schuss liegend und im 3 Stellungskampf an den Start.

Seit diesem Jahr ist die Disziplin 30 Schuss stehend hinzugekommen und erfreut sich großer Beliebtheit.

Das Kleinkaliberschießen ist mittlerweile wieder ein fester Bestandteil unseres Schießprogrammes und erfreut sich einer steigenden Anzahl von Schützen.

