---
title: Freie Pistole
bannerImage: /images/fp.jpg
subTitle: Die Königsdisziplin
showFullAsDetail: true
subMenuOrder: 5
---
## Fakten
- 8 konventionellen Schießstände
- 50 Meter

Die Sportart der „freien Pistole“ wird in Sportschützenkreisen als eine Königsdisziplin bezeichnet, u.a. weil sie schon wegen der langen Wettkampfdauer von zwei Stunden so dauerhaft Konzentration verlangt und abweichend von der Sportpistole die auf eine Entfernung von 25 Metern geschossen auf eine Entfernung von 50 Metern geschossen wird.

Diese anspruchsvolle Disziplin erfreut sich gerade wegen ihrer Komplexität aktuell steigender Beliebtheit in unserem Verein und wird regelmäßig im Rahmen geführter Trainingseinheiten angeboten.
