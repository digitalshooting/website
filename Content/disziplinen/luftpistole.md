---
title: Luftpistole
subTitle: Zielgenau auf 10 Meter
bannerImage: /images/lp.jpg
showFullAsDetail: true
subMenuOrder: 2
---
## Fakten
- 12 elektronischen Schießstände
- 12 konventionellen Schießstände

Das Luftpistolenschießen stand in der Vergangenheit immer im Schatten des Luftgewehrschießens. Mittlerweile hat sich das aber geändert. Im Gegensatz zum jugenddominierten Luftgewehrschießen bietet diese Disziplin vor allem Erwachsenen die Möglichkeit den Schießsport für sich zu entdecken. Zur Zeit sind in unserem Verein 21 Schützen im Bereich Luftpistole aktiv.

An den Rundenwettkämpfen des Schützengaues Main-Spessart sind wir mit 4 Luftpistolenmannschaften in verschiedenen Klassen erfolgreich vertreten.
