---
title: Sportpistole
subTitle: Kaliber 22
bannerImage: /images/sp.jpg
showFullAsDetail: true
subMenuOrder: 4
---
## Fakten
- 4 konventionellen Schießstände - 25m
- 2 Mannschaften

Das Schießen mit der Sportpistole Kaliber 22 ist seit Sommer 1999 ein fester Bestandteil unseres Sportbetriebes.

Auf 4 Sportpistolenständen besteht für die Schützen die Möglichkeit ihrem Sport in den Disziplinen Präzision und Duell nach zu gehen.

Da bei uns die Sicherheit an erster Stelle kommt, absolvieren Kurzwaffenschützen ohne Schießpraxis mindestens 10 Trainingseinheiten unter fachkundiger Anleitung der Referatsleiter mit der Luftpistole, bevor sie am Training mit der Sportpistole teilnehmen.

Aktuell ist Diana Dettingen mit 2 Sportpistolenmannschaften bei den Rundenwettkämpfen des Schützengaues Main Spessart vertreten.
