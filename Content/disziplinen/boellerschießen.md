---
title: Böllerschießen
subTitle: Wir lassen`s krachen - Traditon pflegen
bannerImage: /images/boeller.jpg
showFullAsDetail: true
subMenuOrder: 6
---
## Fakten
- Tradition
- Festlich
- Geschichte

Der Brauch des Böllerschießens reicht bis in das 15. Jahrhundert zurück. Auch bei uns in Dettingen wurde schon um die Jahrhundertwende kräftig geböllert.

Der Schützenverein „DIANA“Dettingen hat es sich zur Aufgabe gemacht, dieses alte Brauchtum und die Traditionen weiterleben zu lassen und daher im Jahr 1998 eine Böllergruppe gegründet.

Geböllert wird zu verschiedensten Anlässen, wie zum Beispiel bei großen Jubiläen oder Hochzeiten von Mitgliedern. Ferner, wenn es gewünscht wird, bei gemeindlichen, kirchlichen und vereinsspezifischen Anlässen, sowie bei regionalen und überregionalen Ereignissen aus dem Schützenwesen.

Für weitere Fragen wenden Sie sich gerne an unseren Böllerkommandant Andreas Hieser andi.hieser@web.de oder schauen Sie Freitagabends ab 20:00 Uhr in unserem Schützenhaus vorbei.

