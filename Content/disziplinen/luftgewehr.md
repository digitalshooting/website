---
title: Luftgewehr
subTitle: Die Luftgewehrsparte ist die größte Abteilung in unserem Verein
bannerImage: /images/lg_2.jpg
showFullAsDetail: true
subMenuOrder: 1
---
## Fakten
- 12 elektronischen Schießstände
- 12 konventionellen Schießstände
- 4 ausgebildete Jugendtrainer
- 3 Fachtrainer 

Aktuell sind wir erfolgreich mit 7 Mannschaften an den Rundenwettkämpfen des Schützengaues Main Spessart und des Bezirkes Unterfranken vertreten.

Mit unseren 12 elektronischen und 12 konventionellen Schießständen finden unsere zahlreichen Schützen optimale Bedingungen vor. Die Erfolge im Luftgewehrschießen basieren zum großen Teil auf unserer erfolgreichen und nachhaltigen Jugendarbeit für die der Verein im Jahr 2016 den H&N Förderpreis, der von der Deutschen Schützenjugend und der Firma H&N ausgeschrieben wird gewinnen konnte.

Um auch in Zukunft erfolgreiche Sportarbeit leisten zu können, stehen den Schützen 4 ausgebildete Jugendleiter nach der Richtlinien des Bayerischen Staatsministeriums für Unterricht, Kultus, Wissenschaft und Kunst sowie 3 Fachtrainer und zahlreiche Vereinsübungsleiter zur Verfügung.
