---
title: Vorstandschaft
showFullAsDetail: true
subMenuOrder: 2
hideBorder: true
---
| Vorstandschaft    |     |
| -------------------------- | ------------- |
| 1. Schützenmeister      | Christopher Dorsey (<a href="mailto:cdorsey@diana-dettingen.de">cdorsey@diana-dettingen.de</a>) |
| 2. Schützenmeister      | Sebastian Uschek (<a href="mailto:suschek@diana-dettingen.de">suschek@diana-dettingen.de</a>) |
| 1. Schatzmeister      | Ulrike Uftring (<a href="mailto:schatzmeister@diana-dettingen.de">schatzmeister@diana-dettingen.de</a>) |
| 2. Schatzmeister      | Petra Baumann |
| Schriftführer      | Karsten Küchler |
| Sportleiter      | Hans Peter Wienand |
| Jugendleiter      | Andreas Hieser |
| Mitgliedsverwaltung      | Jannik Lorenz (<a href="mailto:jlorenz@diana-dettingen.de">jlorenz@diana-dettingen.de</a>) |
