---
title: Referenten
showFullAsDetail: true
subMenuOrder: 3
hideBorder: true
---
| Referenten  |   |
| -------------------------- | ------------- |
| Pistole (WBK-pflichtig)   | Sebastian Uschek |
| Luftgewehr   | Hans Peter Wienand |
| Luftpistole   | Sabine Stein |
| Kleinkalibergewehr   | Rebecca Steurer |
| Böller   | Andreas Hieser |
