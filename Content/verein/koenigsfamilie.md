---
title: Königsfamilie
subTitle: Die Königsfamilie 2022 des Schützenverein Diana Dettingen stellt sich vor.
bannerImage: /images/2022-koenig.jpg
showFullAsDetail: true
subMenuOrder: 4
---


Schützenkönig ist *Sebastian Uschek*. Ihm zur Seite stehen der 1. Ritter *Tobias Kohnle* und der 2. Ritter *Lukas Lang*.

*Lichtgewehr-König* wurde der jüngste Teilnehmer *Jonas Kohnle*, zur Seite stehen ihm 1. Ritter *Robin Hof* und 2. Ritter *Lukas Kohnle* 10,3 Ringen.

Neue und alte Jugendkönigin ist *Angelina Welker*, gefolgt vom 1. Jugendritter *Eileen Heeg*.

Den Reinhold-Köppel-Pokal in der Disziplin Luftpistole gewann *Peter Uschek*, in der Disziplin Luftgewehr gewann *Sabine Stein*.

Der diesjährige Königspokal, nahm *Jannik Lorenz* mit dem besten Teiler des Abends mit nach Hause

Sportpistolenprinz wurde *Horst Bott*.

Luftpistolenprinz wurde *Daniel Bergmann*, ihm stehen sein  1. Knappe *Hans-Peter Wienand* sowie als 2. Knappe *Horst Bott* zur Seite.
