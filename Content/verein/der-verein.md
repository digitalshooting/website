---
showFullAsDetail: true
subMenuOrder: 1
hideTitle: true
---
**Der Schützenverein Diana Dettingen ist ein modern ausgerichteter Verein, der seinen Mitgliedern durch die umfangreichen und erfolgreichen Aktivitäten im Bereich des Schießsportes, sowie durch die zahlreichen Freizeitevents und Traditionsveranstaltungen ein breites Spektrum des Vereinslebens bietet.**

Wir zeigen durch unsere zukunftsweisende Ausrichtung, bestehend aus einem Mix aus Breiten- und Leistungssport, sowie einem gesunden Anteil an Tradition, dass der Schießsport nach wie vor modern und attraktiv ist. Die steigenden Mitgliederzahlen, entgegen dem aktuellen Trend, zeigen uns, dass wir hier auf dem richtigen Weg sind und bestärken uns, diesen auch in Zukunft weiter zu beschreiten.

---

## Sport
Wir bieten in den Disziplinen Luftgewehr, Luftpistole, Kleinkalibergewehr, Freie Pistole und Sportpistole optimale Trainingsmöglichkeiten:

- 24 Luftdruckstände (davon12 [vollelektronisch](https://gitlab.com/digitalshooting/dsc))
- 8 Kleinkaliberstände
- 4 Sportpistolenstände

---

## Jugend
Besonders stolz sind wir auf unsere Jugendarbeit!
Diese wurde im Jahr 2016 mit dem Gewinn des von der Deutschen Schützenjugend und der Firma H & N deutschlandweit ausgeschriebenen Förderpreises prämiert!

---

## Tradition
Die Diana-Schützen sind auch im Traditionsbereich vertreten. Hier nimmt zum Beispiel die Böllergruppe an zahlreichen kulturellen Veranstaltungen teil.

---

## Links
- [Gau Main Spessart](http://www.gau-main-spessart.de/)
- [Schützenbezirk Unterfranken](http://www.bssbufr.de/)
- [BSSB](http://www.bssb.de/)
- [DSB](http://www.dsb.de/)

---

## Aufnahmeantrag
- [Aufnahmeantrag](/dokumente/Aufnahmeantrag.pdf)
