---
title: Ostereierschiessen 2018
subTitle: über 70 Teilnehmer
date: 2018-03-30 00:00
albumPath: /alben/2018_Ostereierschießen/
thumbnail: /alben/2018_Ostereierschießen/o11.JPG
tags: Event
---
Der Veranstaltungskalender des Schützenvereines Diana Dettingen ist auch in diesem Jahr gespickt mit zahlreichen Veranstaltungen. Das alljährliche Ostereierschießen für Vereinsmitglieder und Freunde des Schützenvereins Diana Dettingen am Gründonnerstag ist schon seit Jahren eine feste Größe im Vereinskalender und war auch in diesem Jahr mit über 70 Teilnehmern besonders gut besucht. Nach dem sich alle Anwesenden in die Teilnehmerliste eingetragen hatten, wurden mit Hilfe eines Zufall Generators immer 2 Schützen zusammengelost die gemeinsam als Team in den „Kampf“ um die 600 Ostereier ins Rennen gingen. Um 21:30 Uhr war es dann soweit und die Platzierungen der einzelnen Teams wurden von Andreas Hieser auf der großen Leinwand im Clubraum bekannt gegeben. Das Letzt platzierte Paar bekam jeweils 2 Ostereier und als Trostpreis noch Schokoladenosterhasen überreicht. Die Anzahl der gewonnen Eier steigerte sich mit den erreichten Platzierungen und so konnten die Gewinner des Abends, Reinhold Köppel und Bastian Lindner unter großem Applaus der Gäste jeweils 18 Eier mit nach Hause nehmen.
