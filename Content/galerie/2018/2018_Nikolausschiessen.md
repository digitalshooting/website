---
title: Nikolausschiessen 2018
subTitle: Spass für Jung und Alt
description: Impressionen vom Nikolausschiessen 2018
date: 2018-12-06 00:00
albumPath: /alben/2018_Nikolausschiessen/
thumbnail: /alben/2018_Nikolausschiessen/n9.JPG
tags: Event
---
Am Donnerstag den 06.Dezember stand der Trainingsabend ganz im Zeichen des alljährlichen Nikolausschiessens. Unsere Sportleiterin und Leiterin des Jugendbüros Birgitt, hatte für diesen Abend wieder jede Menge vorbereitet und eingekauft. Anstatt des normalen Trainings standen Schießspiele auf dem Programm. Im Laufe des Abends wurden jede Menge an Süßigkeiten und anderen Leckereien ausgeschossen. Jung und Alt hatten an diesem etwas anderen Trainingsabend ihren Spaß.
