---
title: Weihnachtsfeier 2018
subTitle: über 80 Teilnehmer lassen das Jahr ausklingen
description: Weihnachtsfeier zum Abschluss des Jahres
date: 2018-12-15 00:00
albumPath: /alben/2018_Weihnachtsfeier/
thumbnail: /alben/2018_Weihnachtsfeier/wf22.jpg
tags: Event
---
Am Samstag den 15.Dezember 2018 fand die Weihnachtsfeier der Diana Schützen statt. Auf Grund der über 80 Teilnehmer wurde der Clubraum und die Gastronomieräume genutzt.

Nach dem gemeinsamen Abendessen eröffnete der 1.Schützenmeister Roland Schimmel den offiziellen Teil des Abends mit einem Jahresrückblick. Neben den besinnlichen Worten zum Jahresausklang wurde das vergangene Jahr mit all seinen Facetten Revue passieren lassen. Der Rückblick endete mit den Worten „Wenn wir alle unseren Verein sehen und leben wie es das Wort Verein eigentlich sagt, nämlich uneigennützig, vereint, mit Spaß und Freude etwas gemeinsames erreichen, bin ich mir sicher, dann werden wir noch viel erreichen“.

Danach standen diverse Ehrungen auf dem Programm. Shannen Brückner, die in diesem Jahr ihren 100. Rundenwettkampf ohne einmal zu fehlen geschossen hat, wurde mit einem Glaspokal geehrt. Die Familie Englert, die seit 3 Jahren das Sommerfest der Schützenjugend bei sich zu Hause ausrichtet wurde mit dem Titel „Gastgeber des Jahres“ ausgezeichnet.

Der Vereinstradition folgend, kamen anschließend noch verschiedene Vereinsmitglieder für „erwähnenswerte Leistungen der besonderen Art“ in die Gunst einer speziellen Ehrung.

Danach übernahmen die Jugendsprecher Shannen, Katharina, Georg und Korbinian das Mikrofon und übereichten im Namen der Schützenjugend den Trainern und Betreuer Weihnachtspräsente als kleines Dankeschön für den Einsatz und das Engagement des vergangenen Jahres.

Aber auch die Schützenjugend bekam in diesem Jahr wieder etwas geschenkt. Hier wurden „Packaway Barrel Bags“ mit aufgesticktem Vereinslogo überreicht.

Auch in diesem Jahr durften die Anwesenden wieder an der von Winfried Englert gesponserten „Schinken und Presskopf Schätzung“ teilnehmen. Hier galt es das Gewicht je eines Schinken und eines Presskopfes zu schätzen. Die Gewinner waren hier Petra Baumann und Cirila Brückner.

Der Höhepunkt des Abends war die alljährliche Weihnachtstombola. In diesem Jahr hatten sich die Organisatoren der Tombola richtig ins Zeug gelegt. 200 hochwertige Preise, darunter zahlreiche Kinogutscheine, Gutscheine für verschiedene Gastronomiebetriebe und alles was man so zum Überstehen der Tage zwischen den Jahren braucht. Die Hauptpreise waren eine Busreise für 2 Personen in die Fränkische Schweiz und eine Eintrittskarte für den geplanten Ausflug der Schützenjugend in das Phantasialand 2019.
