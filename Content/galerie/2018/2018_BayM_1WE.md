---
title: Erstes Wochenende der Bayerischen Meisterschaften 2018
subTitle: Trainingsleistungen gut umgesetzt
description: 3 Stellungssschützen mit super Leistungen
date: 2018-06-30 00:00
albumPath: /alben/2018_BayM_1WE/
thumbnail: /alben/2018_BayM_1WE/bm19.JPG
tags: Event
---
Das erste Wochenende der Bayerischen Meisterschaften 2018 in München ist geschafft. Am Samstag den 30.06.2018 ging es für die Schützen Aaron Gollas, Christopher Glombitza, Lukas Lang, Wilhelm und Richard Neumann, Niklas Stein und Katharina Bendix um 06:00 Uhr am Schützenhaus in Dettingen los. Als Betreuer waren der KK Referent Matthias Bendix und der 1.Schüztzenmeister und Jugendleiter Roland Schimmel dabei.
In München angekommen ging es gleich auf die KK Anlage zum 50 Meter stehend schießen. Hier konnten alle Schützen an ihre Trainingsleistungen anknüpfen und schossen die entsprechenden Resultate. Da der eine oder andere mit dem Wind zu kämpfen hatte, wurden auch einige Ringe „liegen gelassen“.

In der offenen Jugendklasse belegte Aaron mit 241 Ringen den 14ten Platz. Bei den Junioren wurden gute Mittelfeldplätze erreicht. Wilhelm Neumann 258 Ringe Platz 32, Lukas Lang 257 Ringe Platz 34, Katharina Bendix 253 Ringe Platz 39, Niklas Stein 237 Ringe Platz46 und Richard Neumann 223 Ringe Platz 50.

Den Abend ließen alle im Biergarten des Lohhofs gemütlich ausklingen, wo die Getränke zur großen Freude der Schützen an diesem Abend von den beiden Betreuern komplett übernommen wurden.

Am Sonntag gingen Aaron Gollas und Chris Glombitza in der Disziplin Luftgewehr 3 Stellung an den Start. Beide Jungs haben die intensiven Trainingseinheiten hervorragend umgesetzt und sehr gute Ergebnisse geschossen. In der Disziplin kniend begann Chris mit 90 und 94. Aaron legte noch einen drauf schoss sehr gute 99 und 94 Ringe. Beim liegend Schießen konnten Aaron mit zwei 99er Serien und Chris mir zwei 98er Serien noch mal gut nachlegen. Zu guter Letzt ging es ans stehend Schießen. Auch hier gaben sich Aaron mit 93 und 95 und Chris mit 94 und 91 keine Blöße und schlossen beide den Wettkampf mehr als zufrieden ab. Aaron konnte 579 Ringe und dem sehr guten 8ten Platz und Chris mit ebenfalls sehr guten 565 Ringen und dem 20ten Platz in einem starken Starterfeld beenden.

Das erste Wochenende der Bayerischen Meisterschaften war für alle Beteiligten ein runde Sache. Es stehen noch 2 weitere Wochenenden mit zahlreichen Wettkämpfen in München vor der Tür. Für das dritte Wochenende, an dem unsere Jüngsten an den Start gehen, ist auch wieder ein Rahmenprogramm geplant.
