---
title: Ferienimpressionen der Schützenjugend 2018
subTitle: Es haben sich alle die Auszeit verdient
date: 2018-08-01 00:00
albumPath: /alben/2018_Ferienimpressionen/
thumbnail: /alben/2018_Ferienimpressionen/f5.jpg
tags: Event
---
Nach dem krönenden Abschluss des Sportjahres durch das große Sommerfest der Schützenjugend ging es für alle in die verdienten Ferien. Jungschützen waren in diesem in Neuseeland, Thailand, Spanien, Kroatien, Österreich, Schweiz, Spanien und in ganz Deutschland unterwegs. Die zahlreichen Impressionen von den verschiedenen Urlaubszielen zeigen, dass sich alle gut erholt haben und somit wieder voller Tatendrang in die neue Saison starten können.
