---
title: Volksfestumzug 2018
subTitle: Mit großer Abordnung dabei
description: Böllerschützen begleiten die Fronleichnamsprozession
date: 2018-06-16 00:00
albumPath: /alben/2018_Volksfestumzug/
thumbnail: /alben/2018_Volksfestumzug/WhatsApp Image 2018-06-16 at 15.38.20.jpeg
tags: Event
---
Am Samstag den 16.Juni 2018 fand der traditionelle Festzug zum Aschaffenburger Volksfest statt.

Da in diesem Jahr die Fahne des Schützengaues Main-Spessart von Diana Dettingen getragen wird, durften wir gleich 2 Fahnenabordnungen stellen. Mit über 20 Teilnehmern, die sich aus Böllerschützen, der Schützenjugend und aktiven Schützen aus den verschiedenen Abteilungen zusammensetzen, hat sich der Schützenverein Diana Dettingen wieder gut präsentiert.
