---
title: Sommerfest der Schützenjugend
description: Am Samstag, den 21 Juli 2018 fand die Saisonabschlussfeier der Dettinger Schützenjugend statt.
date: 2018-07-21 00:00
albumPath: /alben/2018_Sommerfest_Jugend/
thumbnail: /alben/2018_Sommerfest_Jugend/s0.JPG
tags: Event
---
Am Samstag den 21.Juli 2018 fand das mittlerweile traditionelle Sommerfest der Schützenjugend bei Winni in Michelbach statt. Trotz der zahlreichen „Urlauber“ waren wieder mehr als 60 Teilnehmer dabei. Das umfangreiche Grillbuffet ließ wie immer keine Wünsche offen. In diesem Jahr hat sich unser Gastgeber mit einem großen Dart Turnier wieder etwas Neues einfallen lassen. Hier wurde bis zur letzten Minute um den großen Wanderpokal gekämpft. Auch der Vergleich der besten Kuchenbäcker, bei dem der Verlierer anschließend in den Pool durfte war ein Highlight des Abends. Das Sommerfest der Schützenjugend ist der Abschluss der Sportsaison 2018 für die Jungschützen, Eltern, Trainer und Betreuer. Als Ehrengast war in diesem Jahr unser Gauschützenmeister Reinhold Köppel dabei. Jetzt haben sich erst mal alle eine Sommerpause verdient.
