---
title: Abschluss der Bayerischen Meisterschaften 2018
subTitle: Letztendlich waren alle zufrieden
description: 13 Jungschützen an 3 Wochenenden am Start
date: 2018-07-13 00:00
albumPath: /alben/2018_BayM_3WE/
thumbnail: /alben/2018_BayM_3WE/bm48.JPG
tags: Event
---
Am dritten und letzten Wochenende der Bayerischen Meisterschaften 2018 waren mit Emely Gellner, Katharina Bendix, Aaron Gollas, Richard Neumann, Niklas Stein, Jason Brückner, Robin Englert  und Josef Wehner noch mal 8 Jungschützen am Start. Am Freitag den 13.07.2018 ging es um 06:00 Uhr am Schützenhaus in Dettingen los. Nachdem im mittlerweile schon festem „Stammhotel“ Brauerei Gasthaus Lohhof die Zimmer bezogen waren, ging es mit der S-Bahn nach München. Hier stand ein Besuch des Sea Life auf dem Programm, der Abend wurde dann im hoteleigenen Biergarten ausklingen lassen.

Am Samstag standen dann die Luftgewehr Starts von Emely und Aaron auf dem Programm. Den Anfang machte Aaron. In seinem vierten! Start auf dieser Meisterschaft merkte man Aaron doch an, das 3 harte Wochenenden hinter ihm lagen. Aaron schoss für ihn total ungewohnt 12 Achter. Letztendlich konnte er noch gute 369 Ringe erreichen.

Emely ging einen Durchgang später an den Start. Trotz guter Vorbereitung und toller Trainingsergebnisse wollte es an diesem Tag nicht so richtig klappen. In den ersten 3 Serien kam Emely überhaupt nicht in den Wettkampf. Erst in der vierten und letzten Serie fand Emely zu ihrer Form und konnte sogar mit einer guten 10 den Wettkampf beenden. Emely hatte sich zwar wesentlich mehr vorgenommen, musste aber letztendlich mit 362 Ringen zufrieden sein.

Auch hier ging es am Abend wieder in den Biergarten. Die mittlerweile auf 19 Personen angewachsene „Diana Truppe“ bestehend aus Jungschützen, Betreuern und Eltern verbrachte einen tollen Abend im Biergarten. Hier zeigten einige Jungschützen ihr Talent in der Disziplin „Vielfraß“. Es wurden alle Grillplatten und Teller leergeputzt.

Am Sonntagmorgen machten um 08:00 Uhr unsere jüngsten Starter Jason, Robin und Josef in der Disziplin Luftgewehr 3 Stellung den Anfang. Da es für sie die ersten Meisterschaften in München waren, war die Aufregung entsprechend groß. Aber auch die mitgereisten Väter waren „etwas“ nervös. Wie immer ging es mit dem knieend Anschlag los. Hier schossen Jason 174 Ringe, Robin 174 Ringe und Josef 181 Ringe. Alle drei Schützen waren etwas enttäuscht, da sie auf Grund ihrer tollen Trainingsleistungen sich doch etwas mehr erhofft hatten. Beim liegend Anschlag ging es dann wieder besser und so konnten Jason 194 Ringe, Robin 193 Ringe und Josef 189 Ringe für sich verbuchen. Den Abschluss machte das stehend Schießen. Hier konnte Jason seine Trainingsleistung mit 178 Ringen gut umsetzen und den Wettkampf mit guten 539 Ringen beenden. Robin blieb mit 158 Ringen weit unter seinen Möglichkeiten und war trotz aufmunternder Worte doch recht enttäuscht. Auch bei Josef lief das stehend Schießen nicht so wie er es sich gewünscht hatte. Josef erzielt noch mal 168 Ringe und schloss den Wettkampf mit 528 Ringen ab. Für ihren ersten Einsatz auf den Bayerischen Meisterschaften  haben die Jungs ein tolles Ergebnis abgeliefert und haben den Grundstein für die kommenden Jahre gelegt.

Zeitgleich mit unseren 3 Stellungsschülern gingen Katharina, Richard und Niklas in der Disziplin 60 Schuss KK liegend an den Start. Richard einer der Trainingseifrigsten im Bunde wurde leider nicht mit dem entsprechenden Ergebnis belohnt. Er fand  zu keinem Zeitpunkt zu seiner Form und schloss den Wettkampf enttäuscht mit 528 Ringen ab. Niklas hatte zwar auch einige Anlaufschwierigkeiten, konnte sich aber ab der zweiten Serie kontinuierlich steigern und war zum Schluss mit seinen 551 Ringen zufrieden. Katharina schoss einen gleichmäßigen Wettkampf, in den sie nach Aussage ihres Trainers und Vaters zwar einige 8ter zu viel eingebaut hat, lieferte letztendlich gute 556 Ringe ab.

Es waren für alle 13 Jungschützen und für die Betreuer, Eltern und alle die geholfen haben mal wieder 3 anstrengende Wochenenden. Wir haben zwar keine Meistertitel errungen, aber das war auch nicht das Ziel, vielmehr soll die Teilnahme an den Bayerischen Meisterschaften den Jungschützen ein saisonabschließendes Erfolgserlebnis vermitteln, damit sie auch weiterhin die Motivation und den Spaß am Schießsport behalten.

Vielen Dank an alle Helfer und Eltern.
