---
title: Winterzauber in Michelbach bei Winni 2018
subTitle: Wintergrillen für die Jüngsten
date: 2018-01-27 00:00
albumPath: /alben/2018_Wintergrillen_Schueler/
thumbnail: /alben/2018_Wintergrillen_Schueler/wg14.JPG
tags: Event
---
Die Jugendsprecher hatten an diesem Abend zahlreiche Spiele und eine Schnitzeljagd durch Michelbach vorbereitet. Die Jüngsten des Vereines hatten jede Menge Spaß und auch jede Menge Hunger. Dieser wurde mit zahlreichen Leckereien vom Grill gestillt. Zur späteren Stunde gab es noch mal Stockbrot über dem Lagerfeuer. Aber auch für die anwesenden Eltern war der Abend eine tolle Sache. Bei Lagerfeuer und diversen heißen Getränken konnten die Jugendleiter und Betreuer den Eltern zahlreiche Fragen rund um den Schießsport sowie die Jugendarbeit des Vereines beantworten.
