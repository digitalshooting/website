---
title: Königsfeier 2022
subTitle: In diesem Jahr zum 3. Mal in der Lindighalle
description: Sebastian Uschek ist neuer Schützenkönig bei DIANA Dettingen.
date: 2023-03-21 00:00
albumPath: /alben/2022_Königsfeier/
featureOnHome: true
tags: Event
thumbnail: /alben/2022_Königsfeier/IMG_0253.JPG
---
Die Königsproklamation ist der Höhepunkt eines jeden Schützenjahres. Dazu trafen sich am Samstag, dem 18. März 2023, die Mitglieder des Schützenvereins Diana Dettingen e. V. im Foyer der benachbarten Lindighalle. Unterstützt wurde die Veranstaltung von

Winny und Manuela von der Metzgerei Weber, Freigericht-Bernbach, am Königsbuffet
[www.metzgerei-stefanweber.de/](http://www.metzgerei-stefanweber.de/)

dem Team von Kahl fetzt. -  Thekendienst

dem Team des Reit- und Fahrverein Mainaschaff - Service

TWO for YOU - Music and more! - mit musikalischer Unterhaltung
[www.twoforyou.org/](http://www.twoforyou.org/)

Christine Uschek - liebevolle Dekoration und Unterstützung

Am diesjährigen Königsschießen beteiligten sich 59 Schützen-schwestern und -brüder, darunter 9 Jungschützen sowie 5 Kinder beim Ausschießen des Schülerkönigs mit dem Lichtgewehr.

Schützenkönig 2022, Andreas Hieser, hieß die Gäste am Eingang mit einem Sektempfang willkommen.

Los ging es anschließend mit einer kurz gehaltenen Begrüßung der Schützen und der Ehrengäste:
Gau-Ehrenschützenmeister Reinhold Köppel, welcher frisch von den Bezirksmeisterschaften kam, dem Ehrenschützenmeister Reiner Lorenz und dem amtierenden Schützenkönig Andreas Hieser, ehe anschließend durch den amtierenden 2. Ritter Winny das Königsbuffet eröffnet wurde.

Die wenigsten wissen, dass Sportschützen einen Ligabetrieb haben und während einer Saison (meist September bis April) im Zwei-Wochen-Rhythmus einen Wettkampf beim Gegner oder zuhause austragen. Es geht um den Klassenerhalt oder den Abstieg. Für genau diese langjährige Teilnahme am Schießsport, insbesondere an diesen Rundenwettkämpfen, ehrte Sportleiter Hans-Peter Wienand mit nachstehenden Auszeichnungen die folgenden Schützen:
für 25 Jahre aktive Teilnahme am Schießsport wurde Dieter Bauss mit der Nadel in Silber ausgezeichnet.
- für 10 Jahre aktive Teilnahme an Schießsport und mit der bronzenen Nadel ausgezeichnet wurden Lukas Lang, Matthias Lang, Timo Ritter, Rebecca Steurer, Niklas Stein, Sabine Stein.
Anschließend überreichte der Sportleiter die Leistungsabzeichen der letzten Wettkampfrunde 2021/2022 an die zahlreichen  Teilnehmer.

Bevor die Jugendassistentin Sabine Stein und Tobias Kohnle (Jugendbetreuer Lichtgewehr) die Gewinner des erstmals ausgeschossenen Lichtgewehrkönigs für Kinder & Schüler unter 12 Jahren ehrten, lobte die Vorstandschaft an dieser Stelle das ehrenamtliche Engagement von Sabine Stein und Tobias Kohnle.

Als erster König des Abends wurde der "Lichtgewehr-König" proklamiert, es wurde der jüngste Teilnehmer Jonas Kohnle mit 10,5 Ringen, zur Seite stehen ihm 1. Ritter Robin Hof mit 10,3 Ringen, 2. Ritter Lukas Kohnle 10,3 Ringen. Den 4. Platz mit 10,2 Ringen erreichte Leon Hof, den 5. Platz Neuzugang Jane mit tollen 10,1 Ringen.

Neuer und alter Jugendkönig ist Angelina Welker, gefolgt vom 1. Jugendritter Eileen Heeg. Krankheitsbedingt konnte der 2. Ritter nicht bekanntgegeben werden.

Unsere „Reinhold-Köppel-Wanderpokale“, welche vom Namensgeber Reinhold Köppel gestiftet wurden, werden jährlich als Zusatz zu den Königswettbewerben ausgeschossen.
Den Reinhold-Köppel-Pokal für die Disziplin Luftpistole gewann Peter Uschek mit einem 170 Teiler.
Den Reinhold-Köppel-Pokal für die Disziplin Luftgewehr gewann Sabine Stein mit einem 148 Teiler.

Der diesjährige Königspokal, welchen der Sportleiter Hans-Peter Wienand stiftete und der in einer Glasmanufaktur im Bayerischen Wald (Zwiesel) gefertigt wurde, ist mit einer der schönsten Pokale in der Vereinshistorie.
Ausgetragen wurde der Wettkampf um den Königspokal nach dem besten geschossenen Teiler. Wobei aktive Schützen "frei stehend", und nicht aktive "stehend aufgelegt" geschossen haben.
Den Königspokal nahm Jannik Lorenz mit dem besten Teiler des Abends mit nach Hause

Sportpistolenprinz wurde Horst Bott.

Luftpistolenprinz wurde Daniel Bergmann, ihm stehen sein  1. Knappe Hans-Peter Wienand sowie als 2. Knappe Horst Bott zur Seite.

Als „Höhepunkt im Schützenjahr“ bezeichnete die Vorstandschaft die mit Spannung erwartete Proklamation des Schützenkönigs 2023.

Zusammen mit dem Sportleiter Hans-Peter Wienand überreichte der 1. Schützenmeister die Insignien der Königswürde - die Schützenkette - an den neuen Schützenkönig, 2. Schützenmeister Sebastian Uschek, der diese Würde errang. 1. Ritter wurde Tobias Kohnle sowie 2. Ritter Lukas Lang.

1. Schützenmeister
Christopher Dorsey
