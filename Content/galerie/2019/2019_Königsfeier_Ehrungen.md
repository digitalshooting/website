---
title: Ehrungsabend 2019
subTitle: Ehrungen im Rahmen der Königsfeier 2019
description: Ehrungen für sportiche Leistungen und langjährige Verbands und Vereinszugehörigkeiten
date: 2019-03-16 00:00
albumPath: /alben/2019_Koenigsfeier_Ehrungen/
thumbnail: /alben/2019_Koenigsfeier_Ehrungen/e7.JPG
tags: Event
---
Am Samstag den 16.03.2019 fand mit der Königsfeier das jährliche kulturelle Highlight des Schützenvereines Diana Dettingen statt. Auf Grund der über 100 Teilnehmer wurde diese zum zweiten Mal im Foyer der Lindighalle ausgerichtet.

Im aktuellen Jahr standen neben den zahlreichen Sportehrungen auch Ehrungen für langjährige Mitgliedschaften im Verein, als auch im Bayerischen und Deutschen Schützenbund, sowie Auszeichnungen für langjährige schießsportliche Tätigkeiten auf dem Programm.

Um die Verdienste der Mitglieder auch entsprechend zu würdigen und den passenden Rahmen zu geben, wurden diese Mitglieder und Sportler im Rahmen der Königsfeier vom 1. Gauschützenmeister Reinhold Köppel, dem 1. Schützenmeister Roland Schimmel und der Sportleiterin Birgitt Schimmel entsprechend vor großem Publikum geehrt.

Für 10 schießsportliche Jahre wurden Saskia Fleckenstein, Lucia Bandhauer, Fritz Bandhauer und Fabius Ott geehrt.

Für 40 Jahre Vereinsmitgliedschaft wurde Hans Peter Wienand, für 50 Jahre Vereins- und Verbandsmitgliedschaft Reiner Lorenz, Heinz Weigel und Otto Goldhammer geehrt. Der Gauschützenmeister Reinhold Köppel empfing an diesem Abend die Ehrung für 60 Jahre Mitgliedschaft im Bayerischen und Deutschen Schützenbund.

