---
title: Königsfeier 2019
subTitle: Uwe Neumann ist der neue König.
description: Uwe Neumann ist der neue König.
date: 2019-03-16 00:00
albumPath: /alben/2019_Königsfeier/
thumbnail: /alben/2019_Königsfeier/kf0.JPG
tags: Event
---
Uwe Neumann ist neuer Schützenkönig bei DIANA Dettingen

Am Samstag den 16.03.2019 fand mit der Königsfeier das jährliche kulturelle Highlight des Schützenvereines Diana Dettingen statt. Auf Grund der über 100 Teilnehmer wurde diese zum zweiten Mal im Foyer der Lindighalle ausgerichtet.

Im aktuellen Jahr standen neben den zahlreichen Sportehrungen auch Ehrungen für langjährige Mitgliedschaften im Verein, als auch im Bayerischen und Deutschen Schützenbund, sowie Auszeichnungen für langjährige schießsportliche Tätigkeiten auf dem Programm.

Nach den Ehrungen stand die Bekanntgabe der Gewinner, der im Rahmen des Preisschießens ausgeschossenen Sachpreise, auf dem Programm. Die Gutscheine sowie die Geschenkkörbe konnten Karlheinz Brückner, Robin Englert, Sabine Stein, Horst Bott und Sebastian Uschek mit nach Hause nehmen.

Der Höhepunkt des Abends, war die Proklamation der neuen Königsfamilie.

Schützenkönig wurde Uwe Neumann. Ihm zur Seite stehen der 1.Ritter Karlheinz Brückner und der 2. Ritter Korbinian Weis. Pistolenprinz wurde Matthias Lang, Manfred Wissel konnte sich den Titel des Luftpistolenprinzen sichern. Im Jugend- und Juniorenbereich wurde Aaron Gollas Juniorenkönig. Ihm zur Seite stehen die Ritter Katharina Bendix und Christopher Glombitza. Jugendkönig wurde Jason Brückner. Josef Wehner und Fabienne Berninger sind hier die Ritter. Die Wanderpokale Luftpistole und Luftgewehr gingen an Sabine Stein und Lucia Bandhauer. Die Königsscheibe 2019 konnte Christopher Glombitza gewinnen.

Nach der Proklamation wurde mit Livemusik bis in die frühen Morgenstunden ausgelassen gefeiert.

Dank der tollen Vorbereitung der üblichen Helfer im Hintergrund und der Helfer beim Auf- und Abbau war auch die diesjährig Königsfeier eine erfolgreiche Veranstaltung.

