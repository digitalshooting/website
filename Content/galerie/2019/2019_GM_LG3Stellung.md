---
title: Gaumeisterschaft Luftgewehr 3 Stellung 2019
subTitle: Die zweite 3 Stellungsmeisterschaft für unsere Jüngsten
description: Mit 4 Startern dabei
date: 2019-01-26 00:00
albumPath: /alben/2019_GM_LG3Stellung/
featureOnHome: false
tags: Meisterschaft
thumbnail: /alben/2019_GM_LG3Stellung/WhatsApp Image 2019-01-26 at 10.44.46.jpeg
---
Am Samstag den 26.01.2019 fanden auf unserer Anlage im Beisein des Gaujugendleiters Bernd Schneider, die Gaumeisterschaften im Luftgewehr 3 Stellungsschießen statt. Aktuell wird diese Disziplin im Schützengau Main-Spessart nur von Jungschützen aus Schöllkrippen und Dettingen geschossen. Für Dettingen gingen Jason Brückner, Robin Englert, Leo Hohmann und Josef Wehner an den Start. Die Jungschützen waren trotz großer Aufregung voller Elan dabei und konnten die Trainingsleistungen der letzten Wochen gut umsetzen. Die Endergebnisse und Platzierungen werden in den nächsten Tagen online gehen. Dank der tatkräftigen Unterstützung von Rebecca Steurer und Shannen Brückner wurden unsere Teilnehmer super betreut. Für das leibliche Wohl sorgte mal wieder Karlheinz Brückner. Allen Schützen und Betreuern vielen Dank für den Einsatz auf dieser Meisterschaft.
