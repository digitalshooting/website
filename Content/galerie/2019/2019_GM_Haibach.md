---
title: Gaumeisterschaft in Haibach 2019
subTitle: 9 Gaumeistertitel und 9 weitere Podestplätze konnten gewonnen werden
description: 9 Gaumeistertitel und 9 weitere Podestplätze konnten gewonnen werden
date: 2019-01-26 00:00
albumPath: /alben/2019_GM_Haibach/
featureOnHome: false
tags: Meisterschaft
thumbnail: /alben/2019_GM_Haibach/gm15.JPG
---
Am Samstag den 26.01.2019 fanden die Gaumeisterschaften Luftgewehr 3 Stellung in Dettingen und Samstag den 02.02.2019 und am Sonntag den 03.02.2019 in der Kultur- und Sporthalle Haibach die Gaumeisterschaften in den weiteren Luftdruckdisziplinen des Schützengaues Main Spessart statt.
Die Diana Schützen konnten in diesem Jahr 17 Jungschützen und 10 Erwachsene in den Disziplinen Luftgewehr und Luftpistole an den Start bringen. Die 27 Starter konnten 9 Gaumeistertitel und 9 Podest Plätze in den Einzel- als auch in den Mannschaftsdisziplinen gewinnen und starteten erfolgreich in das neue Meisterschaftsjahr. Die für die Kaderschützen Main-Spessart startenden Vereinsmitglieder konnten weitere 4 Gaumeistertitel und 3 Podestplätze gewinnen.
