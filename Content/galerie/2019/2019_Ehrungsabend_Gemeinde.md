---
title: Ehrungsabend der Gemeinde Karlstein 2019
subTitle: 8 Diana Schützen dabei
description: Gelungenen Abend in der ehemaligen Kultur und Sporthalle in Dettingen
date: 2019-03-02 00:00
albumPath: /alben/2019_Ehrungsabend_Gemeinde/
thumbnail: /alben/2019_Ehrungsabend_Gemeinde/EA4.JPG
tags: Event
---
Am Sonntag den 03.02.2019 fand die Sportlerehrung der Gemeinde Karlstein statt. Unser Bürgermeister Peter Kress übereichte unseren Jungschützen als Dank der Gemeinde Karlstein für ihre Erfolge im vergangenen Jahr Urkunden und Preise. Der Musikverein Großwelzheim gab dem Abend einen würdigen Rahmen. Anette Wolf und Rebecca Steurer waren leider beide verhindert.

Nach der Ehrung traf man sich im Schützenhaus zu einem gemeinsamen Abendessen.
