---
title: Spielabend der Jugend 2019
subTitle: Eindrücke vom Spieleabend der Jugend 2019
description: Eindrücke vom Spieleabend der Jugend 2019
date: 2019-01-26 00:00
albumPath: /alben/2019_Spieleabend/
featureOnHome: false
tags: Event
thumbnail: /alben/2019_Spieleabend/spieleanbend2.JPG
---
Am Samstag den 26.Januar 2019 fand der Spieleabend der Schützenjugend statt. Der Spieleabend, der von den Jugendsprechern durchgeführt wurde, war hauptsächlich für die Vereinsjüngsten gedacht. Aber auch die anwesenden „Großen“ hatten ihren Spaß dabei. Vor allem das Online Ratespiel das von Katharina Bendix vorbereitet worden war, sorgte für jede Menge Spaß. Die Verpflegung übernahm unsere Wirtin Viki. Die große Schnitzelplatte mit reichlich Pommes fand bei den Jungschützen reißenden Absatz.
Aber auch die Eltern und Betreuer hatten im Rahmen eines „Elternstammtisches“ einen schönen Abend. Der Elternstammtisch fand so einen großen Anklang, dass hier ein fester Turnus angedacht wird an dem sich die Eltern und Betreuer austauschen können. Hier sind aber auch Interessierte herzlich willkommen.

Zum Schluss waren sich alle einig, dass dieser Spieleabend, übrigens ganz ohne Handy, (diese wurden vorher eingesammelt) wiederholt werden muss.
