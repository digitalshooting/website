---
title: Kontakt
showFullAsDetail: true
subMenuOrder: 1
bannerImage: /images/anlage.jpg
---
## Schützenhaus/ Schießanlage
````
Am Friedhof 9
63791 Karlstein
````

### Abfahrt Karlstein, Alzenau - Süd
````
auf die St2434
nach ca. 1km rechts in die Frankenstaße
nach ca. 800m links in die Friedensstraße
gerade aus bis zum Schützenhaus
````

### Abfahrt Karlstein, Kleinostheim
````
auf die B8
nach ca. 1,5km rechts in die Frankenstaße
nach ca. 800m rechts in die Friedensstraße
gerade aus bis zum Schützenhaus
````

### GPS
````
50.036748926083156
9.041141867637634
````

---

## Vereinsanschrift

````
Christopher Dorsey
Antoniusstraße 4
63741 Aschaffenburg
````
<a href="mailto:schuetzenmeister@diana-dettingen.de">schuetzenmeister@diana-dettingen.de</a>
