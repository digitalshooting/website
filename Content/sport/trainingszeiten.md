---
title: Trainingszeiten
subTitle: Geist, Kraft und Ruhe auf einen Nenner bringen.
bannerImage: /backgrounds/7.jpg
showFullAsDetail: true
subMenuOrder: 1
---
| Disziplinen | Altersklasse | Wochentag  |   Uhrzeit   |  Ansprechpartner |
| ------------ | --------------- | -------------- | ----------- | ---------------------- |
| Luftgewehr |  Schüler/Jugend/Junioren  |   Dienstag  |   18:00 Uhr   |  Hans-Peter Wienand |
| Luftgewehr | Schützenklasse                 |    Mittwoch  |   20:00 Uhr   |  Hans-Peter Wienand |
| Luftpistole | Alle                                     |  Mittwoch  |   20:00 Uhr    | Sabine Stein |
| Sportpistole | Alle                                   |     Mittwoch/Freitag   |  18:00 Uhr    | Sebastian Uschek |
| Kleinkaliber  | Alle                                 |  nach Vereinbarung     |                    |      Rebecca Steuere |
<br>
Abweichende Trainingszeiten nach Abstimmung mit der Sportleitung und den Ansprechpartnern.
