---
date: 2022-06-10 00:00
description: Am Freitag, 10. Juni 2022 findet unsere diesjährige Rundenwettkampfabschlussfeier statt.
tags: RWK, Termin
featureOnHome: false
---
# Einladung zur Rundenwettkampfabschlussfeier

wir möchten Euch gerne zu unserer diesjährigen Rundenwettkampfabschlussfeier im Schützenhaus einladen.
Es gibt leckere Steaks und Bratwürste vom Grill.
Das ganze findet am Freitag, den 10.Juni.2022 ab 18 Uhr im Schützenhaus Diana Dettingen statt.
Um besser planen zu können meldet Euch bitte bis zum 03.Juni.2022 bei der Vorstandschaft an.

![Download](/2022-06-10_RWK_Abschluss.jpg)
[Download](/2022-06-10_RWK_Abschluss.pdf)
