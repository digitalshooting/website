---
title: Ostereierschießen 2023
subTitle: Gründonnerstag den 6. April 2023 ab 19:00 Uhr.
description: Für Kinder und Schüler bis 12 veranstalten wir davor von 17:00 Uhr bis 18:00 Uhr ein Ostereierschießen mit dem Lichtgewehr.
date: 2023-04-06 00:00
featureOnHome: true
tags: Event
---

Gründonnerstag den 6. April 2023 ab 19:00 Uhr.

Bitte beachtet das Wir für Kinder und Schüler (bis 12) von 17:00 Uhr bis 18:00 Uhr ein Ostereierschießen mit dem Lichtgewehr (Aufgelegt) ausrichten.
Solltet Ihr bekannte oder freunde haben für deren Kinder das etwas sein könnte, können diese gerne gegen beiliegende Anmeldung in der Einladung Teilnehmen.
Die Anmeldung gilt nur für das Lichtgewehrschießen, da die Kids andere Körbchen bekommen.


[![](/Einladung_Ostereierschießen_2023.png) Einladung & Anmeldung](/Einladung_Ostereierschießen_2023.pdf)
