---
showFullAsDetail: true
subMenuOrder: 2003
bannerImage: /chronik/brand.jpg
---
# Der wohl schwärzeste Tag in der Vereinsgeschichte!
## 4.8.2003
In den frühen Morgenstunden brennt unser Vereinslokal fast völlig aus. Nur dem schnellen Handeln der Feuerwehren aus Dettingen und Großwelzheim ist es zu verdanken, dass unser Anwesen nicht bis auf die Grundmauern niedergebrannt ist. Die Kriminalpolizei stellt eindeutig als Ursache Brandstiftung fest. Der oder die Täter haben vorher bei einem Einbruchdiebstahl auch die Wirtewohnung verwüstet. 

Auf Grund des Brandschadens, welcher mit ca. 250 000.- € zu beziffern ist, wird der bestehende Pachtvertrag aufgelöst. Die Aufbauarbeiten beginnen am 2.9.2003. In Eigenleistung wird der Küchentrakt um 3 Meter erweitert.
