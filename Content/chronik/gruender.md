---
showFullAsDetail: true
subMenuOrder: 1
bannerImage: /chronik/gruender.jpg
hideTitle: true
---

Damals kann von einem modernen Schießbetrieb, wie wir ihn heute kennen, natürlich nicht die Rede sein. Die Gaststätte ist Dank dem Vereinswirt Wilhelm Löser gleichzeitig Vereinslokal und Schießstätte. Kurt Stock kauft ein *für Wettkämpfe geeignetes Gewehr*, welches er dem Verein zur Verfügung stellt.
Im Obergeschoss des Gasthauses schafft man durch Öffnen einer Wand zwischen einem kleinen Saal und dem angrenzenden Fremdenzimmer die Möglichkeit, 2 Schießstände (später 4) zu installieren.

Die ersten Verbandsmeisterschaften werden somit über die Betten des Fremdenzimmers ausgetragen – mit dem Nachteil, dass der Vereinswirt bei Heimkämpfen und Trainingsabenden keine Übernachtungsgäste aufnehmen kann. Später wird die vorhandene Kegelbahn zu Schießständen umgebaut.
Inzwischen erlangt der junge Verein die Anerkennung auf Gemeinnützigkeit und ist dem Bayerischen Sportschützenbund beigetreten. (6.10.1954).
