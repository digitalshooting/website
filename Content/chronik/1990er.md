---
showFullAsDetail: true
subMenuOrder: 1990
htmlTag: 1990
---
## 1990
Reiner Lorenz wechselt vom Vergnügungsausschuß in das Schützenmeisteramt – er wird 2. Schützenmeister. Peter Reuther übernimmt die Jugendabteilung. Im Bereich der Josef Ritschel-Schießanlage wird eine Dachsanierung durchgeführt.

12 Luftgewehrstände sind inzwischen mit elektrischen Zuganlagen der Firma Häring ausgestattet. Nach 5-jähriger Pause findet wieder ein Ortsvereinsschießen statt. Zum ersten Mal ermittelt der Verein seinen Schützenkönig mittels eines Adlerschießens auf der KK-Anlage. 5 im Zentrum der Adlerscheibe versteckte Sprengladungen sorgen für Spannung. Die Proklamation findet im Rahmen des vorerst letzten Königsballs in den Räumen der Kultur- und Sporthalle Dettingen statt. Die älteren Schützenbrüder, liebevoll *Oldie-Schützen* genannt, treffen sich mit Eifer alle 14 Tage zum KK-Schießen. Die 1. Luftgewehrmannschaft steigt aus der Bezirks-Oberliga ab.

---

## 1991
Die Pächterwohnung erhält neue Fenster. Statistik: 85 Mitglieder.

---

## 1992
In Vereinsunion wird mit dem Pächterehepaar ein *kleines Schützenfest* veranstaltet.
Kostenvoranschläge zur Terrassenüberdachung werden eingeholt.

Beim Königsschießen auf den Adler tritt Werner Meinl an den Stand und verkündet scherzhaft: Jetzt schießt der König - und es schoss der König! Sehr gesellig geht es zu auf unserem Vereinsausflug zur Weinprobe nach Gau-Algesheim. Den Jahresabschluss bildet ein Brezelschießen.

---

## 1993
Wechsel in der Vorstandschaft. Im Schützenmeisteramt werden die Plätze getauscht.
Reiner Lorenz wird 1. Schützenmeister, Theo Kneisel sein Vertreter und Thomas Merget löst Heinz Weigl als Kassier ab. Für die *rundenkampffreie Zeit* wird auf Anregung von Wolfgang Niemann ein *Duettschießen* ins Leben gerufen. Peter Reuther organisiert eine Fahrradrallye. In einem KK-Freundschaftskampf gegen Perth (Schottland) verlieren wir mit 26 Ringen. Umfangreiche Sanierungsarbeiten am Abwasserkanal zum Schützenhaus kosten wiederum Zeit und Geld. Marianne Weigl wird als erste Frau *DIANA*-Schützenkönigin. Die erste Ausgabe der Vereinszeitung *DIANA-DIALOG* erscheint an der Weihnachtsfeier.

---

## 1994
Im Rahmen der Jahreshauptversammlung werden die verbliebenen Gründungs- und Ehrenmitglieder Walter Bergmann, Herbert Dehn, Ewald Hussi, Alfred Herzog, Burkhard Sittinger und Wolfgang Niemann für 40-jährige Vereinszugehörigkeit geehrt. Eine großflächige Terrassenüberdachung wird im Frühjahr fertiggestellt.

Zum Jubiläums-Königsball am 29.10. in der Kultur- und Sporthalle spielt die Tanzkapelle Werner Hengge. Michael Kneisel wird mit 8 Jahren Jungschützenkönig.

---

## 1995
Ein Genehmigungsverfahren zum Bau einer Pistolenanlage wird eingeleitet. Mittels eines neu angebauten Treppenhauses können nun beide Schießstandebenen ohne störenden Umweg durch die Gaststätte erreicht werden. Mit den Tell-Schützen aus Kleinostheim veranstalten wir einen gemeinsamen Vereinsausflug.

Karl Heinz Unkelbach schießt Standrekord (391 Ringe am 24.2.1995)

---

## 1996
Werner Unkelbach übernimmt das Amt des Sportleiters. Auf Gau-Ebene tritt Reinhold Köppel an die 1. Stelle und löst Josef Kunkel als 1. Gauschützenmeister ab. Das Schützenhaus erhält einen neuen Außenanstrich, der Treppenhausanbau wird fertiggestellt. Wir veranstalten eine gemeinsame Königsfeier mit Tell-Kleinostheim. Die Schießsportgemeinschaft mit den Tell-Schützen wird jedoch aufgelöst.

---

## 1997
Der Wunsch nach einer eigenen Vereinsfahne reift in den Köpfen der Mitglieder. Reiner Lorenz legt dem Vereinsausschuss erste Entwürfe vor. Das Landratsamt Aschaffenburg signalisiert im März 97 grünes Licht für den Umbau unserer Pistolenanlage. Der Verein organisiert für die Jungschützen eine 3-Tagesfahrt in den Schwarzwald mit einem Besuch im Europa Park Rust. Erstmals zählt der Verein 100 Mitglieder. Das Schützenhaus erhält einen neuen Stromanschluss und wird erdverkabelt. Gleichzeitig wird von Öl auf Gas umgestellt. Eine sportliche Talfahrt beginnt.

---

## 1998
In der Jahreshauptversammlung 1998 wird auf Antrag des 1. Schützenmeisters die Abteilung *Böllerschützen* ins Leben gerufen. Der 1. öffentliche Auftritt der Böllergruppe im Rahmen einer gemeinsamen Königsfeier in Kleinostheim ist von Lampenfieber geprägt. Umfangreiche Elektroinstallationsarbeiten im Schützenhaus werden zum Abschluss gebracht. Der Beginn der Umbauarbeiten (Erweiterung Pistolenstand) beginnt im April. Bezüglich der Drehscheibenanlage wird der Auftrag an die Firma Häring vergeben. Mit der Heylands Brauerei kommt es zu einer neuen Bierbezugsvereinbarung. Das Pokalschießen *Schützenhausscheibe* findet erstmals im Juli statt.

---

## 1999
Der langjährige Vorstand Theodor Kneisel legt das Amt des 2. Schützenmeisters nieder und wird von der Generalversammlung 1999 zum Ehrenschützenmeister ernannt. Andreas Hieser rückt als 2. Schützenmeister in den Vorstand. In der gleichen Mitgliederversammlung wird der Beschluss gefasst, eine Vereinsfahne zu beschaffen. Den Auftrag erhält die Firma Buri aus Würzburg. Zur Einweihung der neuen Pistolenanlage wird ein Sommerfest in Zusammenarbeit mit dem Vereinswirt veranstaltet. Der Verein investiert mehr denn je in seine Jugend. Schießbekleidung und Gewehre werden beschafft. Das fachkundige Training von Andreas Hieser zeigt seine ersten Früchte.
