---
title: Diana Vereinschronik
bannerImage: /backgrounds/SchueHausScheibe.png
bannerShadow: false
showFullAsDetail: true
subMenuOrder: 0
---
<h2>
<center>
  <a href="#1950">1950</a>
   -
  <a href="#1960">1960</a>
   -
  <a href="#1970">1970</a>
   -
  <a href="#1980">1980</a>
   -
  <a href="#1990">1990</a>
   -
  <a href="#2000">2000</a>
   -
  <a href="#2010">2010</a>
</center>
</h2>

---
Eine Chronik soll der Bericht sein über geschichtliche Vorgänge in zeitlicher Reihenfolge.
Wir haben uns bemüht, diesen Abriss aus der Geschichte des Schützenvereins *DIANA* Dettingen so lebendig wie möglich zu gestalten und damit wirklich lebenswert zu machen.

Sie soll eine eindringliche Erinnerung sein an wichtige Ereignisse und gleichzeitig bedeutsame Leistungen aufzeigen, die schließlich den heutigen Verein geprägt haben.

Längst sind die Zeiten dahin, in denen sich die Bürger im Schießen mit der Armbrust übten, um ihre Stadt zu schützen. Die Schützenvereine sind auch nicht mehr wie nach den Befreiungskriegen Träger einer nationalen oder liberalen Bewegung, sondern sie beschränken sich heute allein auf den Schießsport und die Pflege der Geselligkeit sowie alter Traditionen.

---

Am 25.9.1954 gründen 17 mutige Männer unseren Schützenverein “DIANA“ Dettingen mit dem Ziel, das sportliche Schießen zu betreiben.

**<center>Franz Geibig - Walter Bergmann - Karl Langmantel - Lothar Merget - Alfred Herzog - Otto Lang - Herbert Dehn - Hans Krüger - Wilhelm Löser - Konrad Peter - Kurt Stock - Burkhard Sittinger - Fritz Marr - Johann Stein - Erhard Wolf - Helmut Hussi - Ernst Bandhauer</center>**

Die Gründungsversammlung findet im Gasthaus *Löser* (zum Schwanen) in der Hanauer Landstraße statt. In einer ersten geheimen Wahl geht *Franz Geibig* als 1. Schützenmeister hervor. Unterstützt wird er durch den 2. Schützenmeister *Karl Langmantel*, Schriftführer *Walter Bergmann* und Kassier *Alfred Herzog*.
