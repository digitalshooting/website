"use strict";

const cheerio = require("cheerio");
const https = require("https");
const async = require("async");
const pug = require("pug");
const url = require('url');

var config;
var rwk_num;
var sel_turn_date;
if (process.argv.length >= 5) {
  config = require(process.argv[2]);
  rwk_num = process.argv[4];
  sel_turn_date = rwk_num+"|"+process.argv[3];
}
else {
  console.log("Not enougth args")
  process.exit();
}

async.map(config.targets, function(target, callback){
  getTeamList(target, function(teamData){
    getLastRound(target, function(lastRound) {
      getSingleList(target, config.verein, function(singeData){
        callback(null, {
          title: target.title,
          url: target.url,
          teams: target.teams,
          team: teamData,
          single: singeData,
          lastRoundTeams: lastRound,
        });
      });
    });
  });
}, function(err, results) {
  // renderFile
  var html = pug.renderFile("template_report.pug", {
    teams: results,
    type: config.type,
  });
  console.log(html);
});


function getLastRound(target, callback) {
 https.get(target.url, function(res){
    res.setEncoding("utf8");
    let body = "";
    res.on("data", function(data){
      body += data;
    });
    res.on("end", function(){
      const $ = cheerio.load(body);

      var lastRound = [];

      var rounds = $(".turnTable").toArray()
      if (rounds.length > 0) {
        let round;
        if (rwk_num != null) {
          for (var i = 0; i < rounds.length; i++) {
            var r = rounds[i];
            var rwk_num_table = $("b", r).html();
            if (rwk_num_table.includes("Runde "+rwk_num)) {
              round = r;
              break;
            }
          }
        }
        else { round = rounds[0]; }

        var resultTableLink = $(".resultTableLink", round).toArray();
        resultTableLink.forEach(function(resultTL){
          var heim = $(".team", resultTL);
          var gast = heim.next().next();
          var ringHeim = $(".ringsLeft", resultTL);
          var ringGast = ringHeim.next().next();
          
	  heim = heim.contents().html();
          gast = gast.contents().html();
          ringHeim = parseInt(ringHeim.html());
          ringGast = parseInt(ringGast.html());

          if (heim.includes(config.verein) || gast.includes(config.verein)) {
            lastRound.push({
              heim: heim, gast: gast, ringHeim: ringHeim, ringGast: ringGast
            });
          }
        });
      }
      callback(lastRound);
    });
  });
}

function getTeamList(target, callback) {
  var url = target.url + "&sel_turn_date=" + sel_turn_date;
  https.get(url, function(res){
    res.setEncoding("utf8");
    let body = "";
    res.on("data", function(data){
      body += data;
    });
    res.on("end", function(){
      const $ = cheerio.load(body);
  
      // $(".singleResultLink").remove();
      // console.log($.html(".turnTable") );
  
      var summary = $.html(".resultTable");
      if (summary == "") {
        summary = "<p>Es sind noch keine Ergebnisse vorhanden.</p>";
      }
  
      callback(summary);
    });
  });
}


function getSingleList(target, verein, callback) {
  var url_parts = url.parse(target.url, true);
  var url_query = url_parts.query;
  var url_parts_array = url_parts.pathname.split('/');
  var gid = url_parts_array[url_parts_array.length-1];
  
  var post_data = JSON.stringify({
      "gid": gid,
      "sel_group_id": parseInt(url_query.sel_group_id),
      "sel_discipline_id": parseInt(url_query.sel_discipline_id),
      "sel_class_id": parseInt(url_query.sel_class_id),
      "sel_turn_date": sel_turn_date,
      "sel_additional_infos": "0",
      "show_single_results": "1",
      "competition_classes": null,
      "level": 3,
      "archive_group_id": 0,
      "separate_classes": false,
      "ignore_minturns": true
  });
  
  var post_options = {
      host: "www.rwk-onlinemelder.de",
      port: 443,
      path: "/online/api/list/singlelist",
      method: "POST",
      headers: {
          "Content-Type": "application/json",
          'Content-Length': Buffer.byteLength(post_data),
      }
  };
  
  var post_req = https.request(post_options, function(res) {
      res.setEncoding('utf8');
      var data = "";
      res.on('data', function(d) { data += d; });
      res.on('end', function () {
          let single = {
            max_turn_number: 0,
            class_name: "",
            users: []
          };
          try {
            let json = JSON.parse(data);
            single.max_turn_number = json.max_turn_number;
            for (var i in json.list1sorted) {
              single.class_name = json.list1sorted[i].class_name;
              if (json.list1sorted[i].team.includes(verein)) {
                single.users.push(json.list1sorted[i]);
              }
            }
          }
          catch(err) {}
          callback(single);
          
      });
  });
  
  post_req.write(post_data);
  post_req.end();
}

