#!/bin/sh

NODE="node"
SEL_TURN_DATE_1="$1"
SEL_TURN_DATE_2="$2"
RWK_NUM="$3"

cd "/home/dianadet/opt/rwk-onlinemelder-parser"

$NODE genReport.js ./config_lp.js $SEL_TURN_DATE_1 $RWK_NUM > report.html
echo >> report.html
$NODE genReport.js ./config_sportpistole.js $SEL_TURN_DATE_2 $RWK_NUM >> report.html
echo >> report.html
$NODE genReportNextDates.js ./config_lg.js >> report.html
$NODE genReportNextDates.js ./config_lp.js >> report.html
$NODE genReportNextDates.js ./config_sportpistole.js >> report.html
echo >> report.html

export REPLYTO="webmaster@diana-dettingen.de"
mutt -e "set content_type=text/html" -s "$RWK_NUM. LP & Sportpistole Rundenwettkampf" news@list.diana-dettingen.de < report.html
