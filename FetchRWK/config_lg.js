module.exports = {
  type: "LG",
  verein: "Dettingen",
  order: 10,
  targets: [
    {
      title: "Dettingen 1",
      teams: ["Dettingen 1"],
      url: "https://www.rwk-onlinemelder.de/online/listen/800?sel_group_id=32&sel_discipline_id=1&sel_class_id=3&sel_turn_date=&sel_list_type=teamList",
      ical: "https://www.rwk-onlinemelder.de/planung/application/includes/calendar.php?gid=800&team=0&class=3&group=32",
    },
    {
      title: "Dettingen 2",
      teams: ["Dettingen 2"],
      url: "https://rwk-onlinemelder.de/online/listen/802?sel_group_id=34&sel_discipline_id=7&sel_class_id=60&sel_turn_date=&sel_list_type=teamList",
      ical: "https://rwk-onlinemelder.de/planung/application/includes/calendar.php?gid=802&team=0&class=60&group=34",
    },
    {
      title: "Dettingen 3",
      teams: ["Dettingen 3"],
      url: "https://rwk-onlinemelder.de/online/listen/802?sel_group_id=34&sel_discipline_id=7&sel_class_id=62&sel_turn_date=&sel_list_type=teamList",
      ical: "https://rwk-onlinemelder.de/planung/application/includes/calendar.php?gid=802&team=0&class=62&group=34",
    }
  ],
};
