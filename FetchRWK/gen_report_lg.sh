#!/bin/sh

NODE="node"
SEL_TURN_DATE="$1"
RWK_NUM="$2"

cd "/home/dianadet/opt/rwk-onlinemelder-parser"

$NODE genReport.js ./config_lg.js $SEL_TURN_DATE $RWK_NUM > report.html
echo >> report.html
$NODE genReportNextDates.js ./config_lp.js >> report.html
$NODE genReportNextDates.js ./config_sportpistole.js >> report.html
$NODE genReportNextDates.js ./config_lg.js >> report.html
echo >> report.html

export REPLYTO="webmaster@diana-dettingen.de"

# Call with "2019-11-8" "5"
#           Date        Num RWK

mutt -e "set content_type=text/html" -s "$RWK_NUM. LG Rundenwettkampf" news@list.diana-dettingen.de < report.html
