module.exports = {
  type: "LP",
  verein: "Dettingen",
  order: 11,
  targets: [
            {
              title: "Dettingen 1",
              teams: ["Dettingen 1"],
              url: "https://rwk-onlinemelder.de/online/listen/802?sel_group_id=35&sel_discipline_id=13&sel_class_id=85&sel_turn_date=&sel_list_type=teamList",
              ical: "https://rwk-onlinemelder.de/planung/application/includes/calendar.php?gid=802&team=0&class=85&group=35",
            },
            {
              title: "Dettingen 2",
              teams: ["Dettingen 2"],
              url: "https://rwk-onlinemelder.de/online/listen/802?sel_group_id=35&sel_discipline_id=13&sel_class_id=86&sel_turn_date=&sel_list_type=teamList",
              ical: "https://rwk-onlinemelder.de/planung/application/includes/calendar.php?gid=802&team=0&class=86&group=35",
            },
            {
              title: "Dettingen 3",
              teams: ["Dettingen 3"],
              url: "https://rwk-onlinemelder.de/online/listen/802?sel_group_id=35&sel_discipline_id=16&sel_class_id=88&sel_turn_date=&sel_list_type=teamList",
              ical: "https://rwk-onlinemelder.de/planung/application/includes/calendar.php?gid=802&team=0&class=88&group=35",
            }
  ],
};
