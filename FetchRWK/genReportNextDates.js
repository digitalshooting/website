"use strict";

const cheerio = require("cheerio");
const https = require("https");
const async = require("async");
const pug = require("pug");
var ical = require('node-ical');
const url = require('url');

var config;
if (process.argv.length >= 3) {
  config = require(process.argv[2]);
}
else {
  config = require("./config.js");
}





async.map(config.targets, function(target, callback){
  getDates(target, function(dates){
    callback(null, {
      title: target.title,
      url: target,
      dates: dates
    });
  });
}, function(err, results) {

  var d = {}

  // console.log(results)
  results.forEach(function(team){
    team.dates.forEach(function(date){
      var dateString = date[2].getUTCDate() + "." + (date[2].getUTCMonth() + 1) + "." + (date[2].getYear() + 1900);
      if (d[dateString] == undefined) d[dateString] = [];
      d[dateString].push(date)
    });
  });



  var selected = null;
  // Sortierung nach Name
  for (var key in d) {
    var date_arr = key.split(".");
    var date_ob = new Date(date_arr[2], date_arr[1]-1, date_arr[0]-1);

    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate()-1);

    if (date_ob > currentDate && selected == null) selected = key;

    d[key].sort(function(a, b) {
      var x = a[0][0].replace("SV ", "")
      if (a[1] == "Auswärts") x = a[0][1].replace("SV ", "");

      var y = b[0][0].replace("SV ", "")
      if (b[1] == "Auswärts") y = b[0][1].replace("SV ", "");

      if (x < y) {
        return -1;
      }
      if (x > y) {
        return 1;
      }
      return 0;
    });
  }

  // console.log(d)


  // renderFile
  var html = pug.renderFile("template_report_isHome.pug", {
    data: d,
    type: config.type,
    selected: selected,
  });
  console.log(html);
});



function getDates(target, callback) {
  ical.fromURL(target.ical, {}, function(err, data) {
    var events = []
    for (var k in data){
      if (data.hasOwnProperty(k)) {
        var ev = data[k];
        if (ev.summary.indexOf("Dettingen") > -1) {

          // Parse rwk id, and teams
          let teams = ev.summary.substring(9, ev.summary.length).split(" : ");
          let date = ev.end;

          var ort = "Auswärts"
          // if (ev.location.indexOf("63791 Karlstein") > -1) ort = "Heim"
          if (teams[0].indexOf("Dettingen") > -1) ort = "Heim"

          // Parse Ort
          // let arr = ev.location.split(",");
          // if (arr.length >= 2) {
          //   let cityPost = arr[1];
          //   ort = cityPost.substring(7, cityPost.length)
          // }

          events.push([teams, ort, date])
        }

      }
    }
    return callback(events)
  });
}

