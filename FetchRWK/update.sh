#!/bin/sh

NODE="node"
TO="/home/dianadet/web/opt/diana-dettingen.de/templates/layout"


cd "/home/dianadet/opt/rwk-onlinemelder-parser"
$NODE index.js ./config_lg.js > "$TO"/mod_html_rwk_lg.html5
$NODE index.js ./config_lp.js > "$TO"/mod_html_rwk_lp.html5
$NODE index.js ./config_sportpistole.js > "$TO"/mod_html_rwk_sportpistole.html5

$NODE isHome.js ./config_lg.js > "$TO"/mod_html_rwk_termine_lg.html5
$NODE isHome.js ./config_lp.js > "$TO"/mod_html_rwk_termine_lp.html5
$NODE isHome.js ./config_sportpistole.js > "$TO"/mod_html_rwk_termine_sportpistole.html5
