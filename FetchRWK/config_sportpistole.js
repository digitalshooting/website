module.exports = {
  type: "Sportpistole",
  verein: "Dettingen",
  order: 12,
  targets: [
    {
      title: "Dettingen 1",
      teams: ["Dettingen 1"],
      url: "https://rwk-onlinemelder.de/online/listen/802?sel_group_id=36&sel_discipline_id=10&sel_class_id=73&sel_turn_date=&sel_list_type=teamList",
      ical: "https://rwk-onlinemelder.de/planung/application/includes/calendar.php?gid=802&team=0&class=73&group=36",
    },
    {
      title: "Dettingen 2",
      teams: ["Dettingen 2"],
      url: "https://rwk-onlinemelder.de/online/listen/802?sel_group_id=36&sel_discipline_id=10&sel_class_id=75&sel_turn_date=&sel_list_type=teamList",
      ical: "https://rwk-onlinemelder.de/planung/application/includes/calendar.php?gid=802&team=0&class=75&group=36",
    },
  ],
};
