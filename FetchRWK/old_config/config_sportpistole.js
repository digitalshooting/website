module.exports = {
  type: "sportpistole",
  verein: "Dettingen",
  targets: [
    {
      title: "Dettingen 1",
      url: "https://www.rwk-onlinemelder.de/online/listen/802?sel_group_id=20&sel_discipline_id=10&sel_class_id=73&sel_turn_date=&sel_list_type=teamList",
    },
    {
      title: "Dettingen 2",
      url: "https://www.rwk-onlinemelder.de/online/listen/802?sel_group_id=20&sel_discipline_id=17&sel_class_id=77&sel_turn_date=&sel_list_type=teamList",
    },
  ],
};
