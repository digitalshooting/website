module.exports = {
  type: "LP",
  verein: "Dettingen",
  targets: [
    {
      title: "Dettingen 1",
      url: "https://www.rwk-onlinemelder.de/online/listen/802?sel_group_id=19&sel_discipline_id=13&sel_class_id=85&sel_turn_date=&sel_list_type=teamList",
    },
    {
      title: "Dettingen 2",
      url: "https://www.rwk-onlinemelder.de/online/listen/802?sel_group_id=19&sel_discipline_id=16&sel_class_id=90&sel_turn_date=&sel_list_type=teamList",
    },
    {
      title: "Dettingen 3/ 4",
      url: "https://www.rwk-onlinemelder.de/online/listen/802?sel_group_id=19&sel_discipline_id=16&sel_class_id=112&sel_turn_date=&sel_list_type=teamList",
    },
  ],
};
