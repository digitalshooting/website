module.exports = {
  type: "LG",
  verein: "Dettingen",
  targets: [
    {
      title: "Dettingen 1/ 2",
      url: "https://www.rwk-onlinemelder.de/online/listen/800?sel_group_id=21&sel_discipline_id=1&sel_class_id=3&sel_turn_date=&sel_list_type=teamList",
    },
    {
      title: "Dettingen 3",
      url: "https://www.rwk-onlinemelder.de/online/listen/802?sel_group_id=18&sel_discipline_id=7&sel_class_id=60&sel_turn_date=&sel_list_type=teamList",
    },
    {
      title: "Dettingen 4",
      url: "https://www.rwk-onlinemelder.de/online/listen/802?sel_group_id=18&sel_discipline_id=7&sel_class_id=62&sel_turn_date=&sel_list_type=teamList",
    },
    {
      title: "Dettingen 5",
      url: "https://www.rwk-onlinemelder.de/online/listen/802?sel_group_id=18&sel_discipline_id=15&sel_class_id=63&sel_turn_date=&sel_list_type=teamList",
    },
    {
      title: "Dettingen 6",
      url: "https://www.rwk-onlinemelder.de/online/listen/802?sel_group_id=18&sel_discipline_id=15&sel_class_id=68&sel_turn_date=&sel_list_type=teamList",
    },
    {
      title: "Dettingen 7",
      url: "https://www.rwk-onlinemelder.de/online/listen/802?sel_group_id=18&sel_discipline_id=15&sel_class_id=105&sel_turn_date=&sel_list_type=teamList",
    },
  ],
};